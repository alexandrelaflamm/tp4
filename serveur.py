'''
Équipe 28 :

Daba, Anes (111 2555 296)
Desbois, Pierre-Luc (536 782 393)
Laflamme, Alexandre (111 265 474)
'''

import socket
import select
import re
import datetime
import smtplib
import traceback
from email.mime.text import MIMEText
from hashlib import sha512
from os import path, makedirs, listdir
from os.path import getsize
from socketUtils import recv_msg , send_msg

################################################################
#---------------------- Initialisation ------------------------#
################################################################

#init des variables utiles
datetime = datetime.datetime
nomUtilisateur = ""
mdp = ""
listeUtilisateur = []
listeMdp = []

# On prépare deux listes :

# La première pour les sockets des clients connectés
# La seconde pour les sockets en attente d’une opération
listeClients = []
listeAttente = []

#Fonction pour préparer le socket du serveur
def connectServerSocket():
    # On prépare le socket du serveur
    socServeur = socket.socket(socket.AF_INET , socket.SOCK_STREAM) 
    socServeur.setsockopt(socket.SOL_SOCKET , socket.SO_REUSEADDR , 1)
    socServeur.bind(("localhost", 1234)) 
    socServeur.listen()
    return socServeur


# Cette fonction sera appelée lors de la connexion d’un nouveau client
def accueil(socClient):
    (nouveauClient , _) = socClient.accept()
    # On ajoute le nouveau client à la liste des clients
    listeClients.append(nouveauClient)






################################################################
#---------------- Fonctions du premier menu -------------------#
################################################################

############# Fonctions pour la création du compte #############


#Fonction qui sert à créer le dossier d'un utilisateur valide lors de la création de compte
#Fonction qui sert à la création et à la validation d'un nouveau compte client
def creationCompte(socClient):
    nomUtilisateur = creationNom(socClient)
    mdp = creationMdp(socClient)
    creationDossier(nomUtilisateur, mdp)
    send_msg(socClient, "Votre compte est maintenant créé.") 


def creationDossier(nomUtilisateur, mdp):
    hashedMdp = sha512(mdp.encode()).hexdigest() 
    makedirs(nomUtilisateur, exist_ok=True)
    with open(nomUtilisateur+"/password.txt", "w") as mdpFile:
        mdpFile.write(hashedMdp) 
        mdpFile.close()


# Fonction qui sert à la création et validité du nom utilisateur spécifiquement
def creationNom(socClient):
    validite = False
    nomUtilisateur = ""
    while(not validite):
        nomUtilisateur = recv_msg(socClient)
        validite = usernameConforme(nomUtilisateur)
        send_msg(socClient, validite.__str__())
        if (not validite):
            send_msg(socClient, "ERREUR : Le nom d'utilisateur que vous avez entrer est invalide.\nLe nom d'utilisateur ne doit contenir que des lettres, chiffres, "
                            "_ ou . et ne doit pas déjà existé.")
        else:
            send_msg(socClient, "Nom d'utilisateur valide")
    return nomUtilisateur


# Fonction qui sert à la création et validité du mot de passe spécifiquement
def creationMdp(socClient):
    validite = False
    mdp = ""
    while(not validite):
        mdp = recv_msg(socClient)
        validite = mdpConforme(mdp)
        send_msg(socClient, validite.__str__())
        if (not validite):
            send_msg(socClient, "ERREUR : Le mot de passe que vous avez entrer est invalide.\nLe mot de passe doit avoir entre 8 et 20 caractère"
                        " et il doit contenir au moins 1 chiffre, au moins 2 lettres minuscules et 2 lettres masjuscules.\nLes autres"
                        "caracteres ne sont pas permis")
        else:
            send_msg(socClient, "Mot de passe valide")
    return mdp

#Fonction pour vérifier si le mot de passe est conforme
def mdpConforme(stringToSearch):
    isConform = False
    #Au moins 2 lettres majuscules, 2 lettres minuscules et 2 chiffres et pas de caractère spécial
    if(re.search(r"^(?=(.*\d){1,})(?!.*[!@#$%?&*()._])(?=.*[a-z]{2,})(?=.*[A-Z]{2,})[0-9a-zA-Z]{8,20}$", stringToSearch)):
        isConform = True
    return isConform

#Fonction pour vérifier si le nom d'utilisateur est conforme
def usernameConforme(stringToSearch):
    isConform = False
    #pas d'espace ni de caractere spécial et seulement des lettres, chiffres, . ou _
    if(re.search(r"^(?![!@#$%?&*() ])[0-9a-zA-Z._]{1,}$", stringToSearch) and not path.exists(stringToSearch)):
        isConform = True  
    return isConform




############# Fonctions pour la connexion d'un compte #############

#Fonction qui réceptionne le nom d'utilisateur et le mot de passe entré par le 
#client et valide/invalide la connexion si ces informations sont exactes/inexactes
def receptionInfo(socClient):
    nomUtilisateur = recv_msg(socClient)
    mdp = recv_msg(socClient)
    #Le nom d'utilisateur ne peut pas etre nul meme si le mot de passe est bon
    if (userExist(nomUtilisateur) and passwordCheck(mdp, nomUtilisateur)):
        send_msg(socClient, "\nNom d'utilisateur et mot de passe valides : connexion acceptée\n")
        send_msg(socClient, "True")
        return nomUtilisateur, mdp
    else:
        send_msg(socClient, "\nNom d'utilisateur et/ou mot de passe invalide : connexion refusée\n")
        send_msg(socClient, "False")
        return "", ""

#Fonction pour vérifier si l'utilisateur existe
def userExist(stringToSearch):
    if (stringToSearch != None):
        for i in listdir():
            if (i == stringToSearch):
                return True
    return False

#Fonction qui vérifie si le mot de passe entré lors de la connexion est le bon
def passwordCheck(stringToSearch, nomUtilisateur):
    check = False
    if (nomUtilisateur != None):
        with open(nomUtilisateur+"/password.txt", "r") as mdpFile:
            if (sha512(stringToSearch.encode()).hexdigest() == mdpFile.read()):
                check = True
            mdpFile.close()
    return check




################################################################
#---------------- Fonctions du deuxieme menu ------------------#
################################################################

#Fonction pour la consultation de la liste des courriels pour l'utilisateur en question
def consultationCourriel(socClient, nomUtilisateur):
    send_msg(socClient, "#################### DÉBUT DE LA LISTE DES MESSAGES ####################")
    i = 0
    listeSujet = []
    for fileName in listdir(nomUtilisateur):
        #Pas password.txt et seulement les fichiers avec un extensions
        isTxtFile = fileName[fileName.find("."):] == ".txt"
        if (fileName != "password.txt" and isTxtFile):
            i += 1
            listeSujet.append(fileName)
            send_msg(socClient, i.__str__() + '. ' + fileName[:fileName.find(".")])
    send_msg(socClient, "True")
    send_msg(socClient, "#################### FIN DE LA LISTE DES MESSAGES ####################\n")

    selectedFile = int(recv_msg(socClient))
    msg = "Le numéro de fichier choisit ne correspond à aucun fichier"
    if (selectedFile == 0):
        msg = "Aucun fichier à sélectionner. Veuillez réessayer lorsqu'il y aura plus de messages."
    else:
        if (selectedFile <= len(listeSujet)):
            fileName = listeSujet[selectedFile-1]
            if (fileName != None):
                with open(nomUtilisateur+"/" + fileName, "r") as message:
                    msg = "Message :\n" + message.read()
                    message.close()
    send_msg(socClient, msg)


#Fonction pour l'affichage des statistiques liées au nombre de courriel etc.
def affichageStat(socClient, nomUtilisateur):
    #ne sachant pas si l'on doit faire abstraction de password.txt pour la taille du dossier, on a inclu les deux!
    send_msg(socClient, "Nombre de message(s) dans le dossier : " + str(len(listdir(nomUtilisateur))-1))
    send_msg(socClient, "Taille totale du dossier : " + str(sum(getsize(nomUtilisateur + "/" + f) for f in listdir(nomUtilisateur) if (path.isfile(nomUtilisateur + "/" + f)))) + " octet(s)")
    send_msg(socClient, "Taille totale du dossier (sans password.txt) : " + str(sum(getsize(nomUtilisateur + "/" + f) for f in listdir(nomUtilisateur) if (path.isfile(nomUtilisateur + "/" + f) and f != "password.txt"))) + " octet(s)")
    send_msg(socClient, "Liste des messages par sujet : ")

    for fileName in listdir(nomUtilisateur):
        #Pas password.txt et seulement les fichiers avec un extensions
        isTxtFile = fileName[fileName.find("."):] == ".txt"
        if (fileName != "password.txt" and isTxtFile):
            with open(nomUtilisateur+"/" + fileName, "r") as message:
                msg = "Message : " + message.read()
                message.close()
                send_msg(socClient, "Sujet : " + fileName[:fileName.find(".")] + "\n" + msg)
    send_msg(socClient, "True")
    recv_msg(socClient)
    return


############# Fonctions pour l'envoie de courriels #############

#Fonction pour la reception des infos liés à l'envoi d'un courriel
def receptionInfoEnvoiCourriel(socClient):
    addrDest = recv_msg(socClient)
    sujet = recv_msg(socClient)
    corps = recv_msg(socClient)
    return (addrDest, sujet, corps)

#Fonction pour la gestion des courriels avec adresse @ift.glo2000.ca
def courrielIFTGlo(addrDest, sujet, corps):
    #L'utilisateur visé par l'adresse existe
    if (userExist(extractionNom(addrDest))):
        with open(extractionNom(addrDest)+"/"+sujet+".txt","w", encoding="utf8") as messageFile:
            messageFile.write(corps)
            messageFile.close
            send_msg(socClient, "\nLe message a été envoyé avec succès.\n") 

    #L'utilisateur visé par l'adresse n'existe pas
    else:
        makedirs("ERREUR", exist_ok=True)
        with open("ERREUR/"+sujet+".txt","w", encoding="utf8") as errFile: #errFile to differentiate from errorFile
            errFile.write(corps)
            errFile.close
            send_msg(socClient, "ERREUR : l'addresse de destination entrée ne correspond à aucune addresse existante."
            "Le message a été placé dans le dossier ERREUR.") 

#Fonction pour l'envoie d'un courriel STMP
def courrielSTMP(addrDest, sujet, corps, nomUtilisateur):
    #Adresse externe, on utilise stmp.ulaval.ca comme host
    smtpConnection = smtplib.SMTP(host="smtp.ulaval.ca", timeout=10) 
    msg = "Subject : " + sujet + "\n\n" + corps
    smtpConnection.sendmail(nomUtilisateur+"@ift.glo2000.ca", addrDest, msg)
    smtpConnection.quit()

        





################################################################
#------------ Fonctions utilitaires ou commune ----------------#
################################################################

#Fonction pour extrait le nom d'utilisateur d'une adresse courriel
def extractionNom(addrDest):
    return addrDest[:addrDest.find("@")]

def quitter(socClient):
    listeClients.remove(socClient)
    if (nomUtilisateur in listeUtilisateur):
        listeUtilisateur.remove(nomUtilisateur)
    if (mdp in listeMdp):
        listeMdp.remove(mdp)




################################################################
#----------------------- Premier menu -------------------------#
################################################################

def premierMenu(socClient):
    choice = recv_msg(socClient)
    #Processus de création d'un compte
    if (choice == "1"):
        #reception et  validation des infos
        creationCompte(socClient)
        return "",""
        
    #Processus de connexion
    elif (choice == "2"):
        #Reception des infos pour la connexion
        nomUtilisateur, mdp = receptionInfo(socClient)
        return (nomUtilisateur, mdp)

    elif (choice == "3"):
        quitter(socClient)
        return "",""





################################################################
#---------------------- Deuxième menu -------------------------#
################################################################

def secondMenu(socClient, nomUtilisateur, mdp):
    #Choix parmi les 4 choix du menu :
    choice = recv_msg(socClient)
    #Choix 1 : Consultation de courriels
    if (choice == "1"):
        consultationCourriel(socClient, nomUtilisateur)
        
    #Choix 2 : Envoi de courriels
    elif (choice == "2"):

        #Réception des infos pour l'envoi d'un courriel
        addrDest, sujet, corps = receptionInfoEnvoiCourriel(socClient)
        try:

        #Envoi d'un courriel avec une adresse finissant par @ift.glo2000.ca
            if (re.search(r"@ift.glo2000.ca$", addrDest)):

                #Fonction de gestion de l'envoi du courriel par adresse @ift.glo2000.ca
                courrielIFTGlo(addrDest, sujet, corps)

            #Envoi d'un courriel avec le protocole stmp, avec une adresse ayant comme fin stmp.ulaval.ca
            else:

                #Fonction de gestion de l'envoi de courriel par protocole stmp
            
                courrielSTMP(addrDest, sujet, corps, nomUtilisateur)
                send_msg(socClient, "\nMessage envoyé avec succès à l'addresse : "+addrDest+"\n")

        except (smtplib.SMTPException, socket.error, BaseException)as errSend:
            send_msg(socClient, "\nL'envoi du courriel a échoué.\nVous n'êtes peut-être pas connecté au réseau de l'université\n")
            errorFile.write(datetime.now().__str__() + " L'envoi du courriel a échoué\n" + errSend.__str__() + "\n" + traceback.format_exc()+"\n") 
         
            
    #À FAIRE : Choix 3 : Affichage des statistiques
    elif (choice == "3"):
        affichageStat(socClient, nomUtilisateur)

    #Choix 4 : Quitter le programme et se déconnecter du serveur
    elif (choice == "4"):
        quitter(socClient)


try:
    errorFile = open("Error.log", "w", encoding="utf8")
    socServeur = connectServerSocket()
    while True:
        # On passe à select la liste des clients et le socket du serveur
        # On récupère dans la liste d’attente tous les sockets en attente de lecture
        (listeAttente , _, _) = select.select([socServeur] + listeClients , [], []) #c'Est ici qu'on sait qu'il y a une nouvelle connexion, car socket a un nouveau serveur
        # On parcours la liste des sockets en attente
        for i, socClient in enumerate(listeAttente):
            # Si le socket retourné est celui du serveur,
            # un nouveau client est en train de se connecter
            if socClient == socServeur: 
                accueil(socClient)

            # Dans le cas contraire, un client a envoyé un message
            else:
                #La personne est connectée
                if (nomUtilisateur in listeUtilisateur and mdp in listeMdp):
                    secondMenu(socClient, nomUtilisateur, mdp)
                else:
                    nomUtilisateur, mdp = premierMenu(socClient)
                    if (nomUtilisateur != "" and mdp != ""):
                        #La personne a entré des identifiants valides; ses informations sont ajoutées a des listes
                        listeUtilisateur.append(nomUtilisateur)
                        listeMdp.append(mdp)

    if (not errorFile.closed()):
        errorFile.close()

except ConnectionError as errCo :
    message = "Connection Error : une erreur de connection est survenue\n" 
    print("\n" + message + "\n")
    errorFile.write(datetime.now().__str__() + " " + message + errCo.__str__() + "\n" + traceback.format_exc()+"\n")  
    errorFile.close()
except socket.error as sockErr:
    message = "Socket Error : une erreur liée à l'utilisation des sockets est survenue lors de l'exécution du programme\n" 
    print("\n" + message + "\n")
    errorFile.write(datetime.now().__str__() + " " + message + sockErr.__str__() + "\n" + traceback.format_exc()+"\n")  
    errorFile.close()
except OSError as osErr:
    message = "OS Error : une erreur liée à l'OS est survenue lors de l'exécution du programme\n"
    print("\n" + message + "\n")
    errorFile.write(datetime.now().__str__() + " " + message + osErr.__str__() + "\n" + traceback.format_exc()+"\n")  
    errorFile.close()
except BaseException as baseErr:
    message = ("Base Error : une erreur est survenue lors de l'exécution du programme\n")
    print("\n" + message + "\n")
    errorFile.write(datetime.now().__str__() + " " + message + baseErr.__str__() + "\n" + traceback.format_exc()+"\n")  
    errorFile.close()
