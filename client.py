'''
Équipe 28 :

Daba, Anes (111 2555 296)
Desbois, Pierre-Luc (536 782 393)
Laflamme, Alexandre (111 265 474)
'''

import socket
import getpass
import datetime
import traceback
from socketUtils import recv_msg, send_msg

#pour les messages d'erreur
datetime = datetime.datetime

################################################################
#-------------- Fonction pour le premier menu -----------------#
################################################################

#Fonction pour la création d'un compte
def accountCreation():
    print("\nVous avez choisi l'option : 1. Creer un compte\nNous allons maintenant procéder à votre inscription\n")

    #Création et validation du nouveau nom d'utilisateur
    validite = False
    while (not validite):
        send_msg(soc, input("Veuillez créer votre nouveau nom d'utilisateur\n"))
        validite = recv_msg(soc) == "True"
        print(recv_msg(soc))

    #Création et validation du mot de passe
    validite = False
    while (not validite):
        send_msg(soc, getpass.getpass("Veuillez créer votre nouveau mot de passe : \n"))
        validite = recv_msg(soc) == "True"
        print(recv_msg(soc))

    #Message disant que le compte a été créé avec succès
    print(recv_msg(soc))


#Fonction pour la connexion
def accountLogin(firstTime=True):
    if (firstTime):
        print("\nVous avez choisi l'option : 2. Se connecter\nNous allons maintenant procéder à votre connexion\n")
    #Envoie des données au serveur
    send_msg(soc, input("Veuillez entrer votre nom d'utilisateur\n"))
    send_msg(soc, getpass.getpass("Veuillez entrer votre mot de passe : \n"))

    #Message selon si oui ou non, la connexion est acceptée
    print(recv_msg(soc))
    return recv_msg(soc) == "True"




################################################################
#-------------- Fonction pour le deuxième menu -----------------#
################################################################

#Fonction pour l'envoi d'un courriel
def envoiCourriel():
    print("\nVous avez choisi l'option : 2. Envoi de courriels\nNous allons maintenant procéder à la saisie des informations\n")

    #Envoi des infos au serveur
    send_msg(soc, input("Veuillez entrer l'adresse de destination : \n"))
    send_msg(soc, input("Veuillez entrer le sujet du courriel : \n"))
    send_msg(soc, input("Veuillez entrer le message du courriel : \n"))

    #Réception de la réponse
    print(recv_msg(soc))

#Fonction pour la consultation des courriels
def consultationCourriel():
    print("\nVous avez choisi l'option : 2. Consultation de courriels\nNous allons maintenant imprimer votre liste de courriel\n")
    print(recv_msg(soc))
    #On parcourt jusqu'a ce que le serveur dise que c'est terminé
    finish = False
    i = 0
    while (not finish):
        fileName = recv_msg(soc)
        finish = fileName == "True"
        if (not finish):
            i += 1
            print(fileName)
    print(recv_msg(soc))

    if (i > 0):
        send_msg(soc, input("Veuillez entrer le numéro du ficher pour lequel vous désirez consulter le message : \n"))
    else:
        send_msg(soc, "0")
    print("\n"+recv_msg(soc)+"\n")

#Fonction pour l'afichage des statistiques pertinentes
def afficheStat():
    print("\nVous avez choisi l'option : 3. Affichage de statistique\nNous allons maintenant afficher certaines statistiques pertinentes\n")
    print(recv_msg(soc))
    print(recv_msg(soc))
    print(recv_msg(soc))
    print(recv_msg(soc)+"\n")

    finish = False
    while (not finish):
        fileName = recv_msg(soc)
        finish = fileName == "True"
        if (not finish):
            print(fileName + "\n")

    send_msg(soc, input("Lorsque vous êtes prêt à quitter ce menu, veuillez simplement entrer un caractère."))




################################################################
#-------------- Fonction commune aux deux menus ---------------#
################################################################

#Fonction pour quitter le programme et fermer le socket
def quitter(numero):
    print("\nVous avez choisi l'option : " + numero + " Quitter.\nMerci d'avoir utilisé nos services! Au revoir et à la prochaine!\n")
    soc.close





################################################################
#------------------------ Premier menu ------------------------#
################################################################

def premierMenu():
    firstMenu = True
    while(firstMenu):

        #Choix de fonctionnalité
        choice = input("Menu de connexion \n1. Creer un compte\n2. Se connecter\n3. Quitter\n")
        while (not (choice == "1" or choice == "2" or choice == "3")):
            choice = input("Le caractere que vous avez entrez ne correspond à aucun des choix. Veuillez recommencer.\n")
        send_msg(soc, choice)

        #Choix 1 : créer un compte
        if (choice == "1"):
            accountCreation()

        #Choix 2 : se connecter
        elif (choice == "2"):
            secondMenu = accountLogin()

            #pour dire qu'on quitte le premier menu et qu'on aborde le deuxieme
            if (secondMenu):
                firstMenu = False
                deuxiemeMenu()

        #Choix 3 : quitter
        elif (choice == "3"):
            firstMenu = False
            quitter("3")




################################################################
#----------------------- Deuxième menu ------------------------#
################################################################

def deuxiemeMenu():
    #Menu accessible si on se connecte avec succès
    secondMenu = True
    while (secondMenu):

        choice = input("Menu Principal \n1. Consultation de courriels\n2. Envoi de courriels\n3. Statistiques\n4. Quitter\n")
        while (not (choice == "1" or choice == "2" or choice == "3" or choice == "4")):
            choice = input("Le caractere que vous avez entrez ne correspond à aucun des choix. Veuillez recommencer.\n")
        send_msg(soc, choice)

        #Choix 1 : consulter la liste des courriels
        if (choice == "1"):
            consultationCourriel()

        #Choix 2 : envoyer un courriel
        elif (choice == "2"):
            envoiCourriel()

        #À Faire : Choix 3 : affichage des stats
        elif (choice == "3"):
            afficheStat()

        #Choix 4 : quitter
        elif (choice == "4"):
            secondMenu = False
            quitter("4")


try :
    errorFile = open("Error.log", "w", encoding="utf8")
    soc = socket.socket(socket.AF_INET , socket.SOCK_STREAM) 
    soc.connect(("localhost", 1234))

    premierMenu()


except ConnectionError as errCo :
    message = "Connection Error : une erreur de connection est survenue\n" 
    print("\n" + message + "\n")
    errorFile.write(datetime.now().__str__() + " " + message + errCo.__str__() + "\n" + traceback.format_exc()+"\n")  
    errorFile.close()
except socket.error as sockErr:
    message = "Socket Error : une erreur liée à l'utilisation des sockets est survenue lors de l'exécution du programme\n" 
    print("\n" + message + "\n")
    errorFile.write(datetime.now().__str__() + " " + message + sockErr.__str__() + "\n" + traceback.format_exc()+"\n")  
    errorFile.close()
except OSError as osErr:
    message = "OS Error : une erreur liée à l'OS est survenue lors de l'exécution du programme\n"
    print("\n" + message + "\n")
    errorFile.write(datetime.now().__str__() + " " + message + osErr.__str__() + "\n" + traceback.format_exc()+"\n")  
    errorFile.close()
except BaseException as baseErr:
    message = ("Base Error : une erreur est survenue lors de l'exécution du programme\n")
    print("\n" + message + "\n")
    errorFile.write(datetime.now().__str__() + " " + message + baseErr.__str__() + "\n" + traceback.format_exc()+"\n")  
    errorFile.close()